﻿#include <vector>
#include <string>
#include <iostream>
#include <thread>

//https://habr.com/ru/post/188700/


#define PI_PROFILER
#include <chrono>
#ifdef PI_PROFILER
#define PI_PROFILE_SESSION_BEGIN()                                                                   \
        const auto nTimepointA = std::chrono::steady_clock::now()
#define PI_PROFILE_SESSION_END()                                                                  \
        const auto nTimepointB = std::chrono::steady_clock::now();                                           \
        const auto nElapsedTime =                                                                            \
                std::chrono::time_point_cast<std::chrono::milliseconds>(nTimepointB).time_since_epoch() -    \
                std::chrono::time_point_cast<std::chrono::milliseconds>(nTimepointA).time_since_epoch();     \
        std::cout << "Elapsed time: ~" << nElapsedTime.count() << " milliseconds.";
#else
#define PI_PROFILE_SESSION_BEGIN()
#define PI_PROFILE_SESSION_END()
#endif


std::string piSpigot(unsigned int accuracy)
{
    if (accuracy < 1)
    {
        return "please enter a number greater than 0";
    }

    unsigned int size = (accuracy * 10 + 2) / 3;
    std::vector<int> boxes(size, 2);

    std::string pi;
    pi.reserve(accuracy + 1);
    for (unsigned int i = 0; i < accuracy; ++i)
    {
        int carry = 0;
        int sum = 0;

        for (int j = size - 1; j >= 0; --j)
        {
            boxes[j] *= 10;
            sum = boxes[j] + carry;
            int q = sum / (2 * j + 1);
            boxes[j] = sum % (2 * j + 1);
            carry = q * j;
        }

        boxes[0] = sum % 10;
        int q = sum / 10;
        if (q >= 10)
        {
            q = q - 10;

            for (unsigned int j = pi.length() - 1;; --j)
            {
                if (pi[j] == '9')
                    pi[j] = '0';
                else
                {
                    ++pi[j];
                    break;
                }
            }
        }
        pi += ('0' + q);

        if (i == 0)
        {
            pi += '.';
        }
    }
    return pi;
}

int main(int argc, const char* argv[])
{
    unsigned int accuracy = 0;
    std::string pi;
    std::cout << "Input accuracy: ";
    std::cin >> accuracy;

    std::thread t1([&](){
            PI_PROFILE_SESSION_BEGIN();
            pi = piSpigot(accuracy + 1);
            PI_PROFILE_SESSION_END();
        });
    t1.join();
    
    std::cout << std::endl << pi << std::endl;
}